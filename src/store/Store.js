import { createStore } from "vuex";
import axios from "axios";

const store = createStore({
  state: {
    user: null,
    isAuthenticated: false,
    datauser: "",
    datapassword: "",

  },

  mutations: {
    setUser(state, user) {
      state.user = user;
      state.isAuthenticated = true;
    },
    clearUser(state) {
      state.user = null;
      state.isAuthenticated = false;
      state.datauser =  "";
      state.datapassword =  "";
    },
    login(state, user) {
      state.user = user
      state.isAuthenticated = true

      console.log(state)

      // Store the user's information in localStorage
      localStorage.setItem('user', JSON.stringify(user))
      localStorage.setItem('isAuthenticated', true)
    },
    logout(state) {
      state.user = null
      state.isAuthenticated = false
      // Remove the user's information from localStorage
      localStorage.removeItem('user')
      localStorage.removeItem('isAuthenticated')
    },
  },

  actions: {
    async login({ commit }, { email, password }) {
      if (!email || !password) {
        alert("Please enter your email and password.");
      }
      try {
        // make API call to authenticate user and fetch data
        const response = await axios.post("http://localhost:3000/users/login", {
          email,
          password,
        });
        this.datauser = email;
        this.datapassword = password;

        const user = response.data.user;
        const data = response.data.data;
        
        // set user and data in store
        commit("setUser", user);
        commit("setData", data);
        console.log(this.datauser + " " + this.datapassword);
        // return success message
        return "Login successful!";
      } catch (error) {
        alert("email and password not found.!");
        // handle error
        throw new Error("Invalid email or password");
      }
    },
    logout({ commit }) {
      this.datauser = "";
      this.datapassword = "";
      // clear user information from the store
      commit("clearUser");
    },
  },
});

export default store;