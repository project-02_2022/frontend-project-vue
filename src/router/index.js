import { createRouter, createWebHistory } from "vue-router";
import ConferenceTable from "../views/Admin/Dashboard/ConferTable";
import CreatePaperAdmin from "../views/Admin/Form/CreatePaperAdmin";
import ConferenceDetail from "../views/Admin/Dashboard/ConferenceDetail";
import CreateConferenceAdmin from "../views/Admin/Form/CreateConferenceAdmin";
import PaperDetail from "../views/Admin/Dashboard/PaperDetail";
import FormCoordinator from "../views/Coordinator/Form/FormCoordinator";
import TestPage from "../views/TestPage";
import RegirationPaperTable from "../views/Author/Form/RegirationPaperTable";
import RegirationPaperName from '../views/Author/Form/RegirationPaperName'
import GraphCategory from "../views/Admin/Dashboard/GraphCategory";
import PaperAllName from "../views/Admin/Dashboard/PaperAllName";
import NewCategoryAdmin from "../views/Admin/Form/NewCategoryAdmin";
import CategoryCoordinator from "../views/Coordinator/Form/CategoryCoordinator";
import FormRegiration from "../views/Author/Form/FormRegiration";
import TableConferencesCoordinator from "../views/Coordinator/Form/TableConferencesCoordinator.vue";
import NotFound from "../views/NotFound";
import LoginSystem from "../views/Admin/Login/LoginSystem.vue";
import store from "../store/Store";
import SystemDisplay from "../views/SystemDisplay"
import SeePaperAll from '../views/Admin/Dashboard/SeePaperAll'
const routes = [
  {
    path: "/",
    name: "ConferenceTable",
    component: ConferenceTable,
    meta: { requiresAuth: true },
  },
  {
    path: "/createpaperadmin/:id",
    name: "CreatePaperAdmin",
    component: CreatePaperAdmin,
    meta: { requiresAuth: true },
  },
  {
    path: "/conferencedetail/:id",
    name: "ConferencesDetail",
    component: ConferenceDetail,
    meta: { requiresAuth: true },
  },
  {
    path: "/createconference",
    name: "CreateConference",
    component: CreateConferenceAdmin,
    meta: { requiresAuth: true },
  },
  {
    path: "/seeallpaper",
    name: "SeeAllPaper",
    component: SeePaperAll,
    meta: { requiresAuth: true },
  },
  {
    path: "/login",
    name: "Login",
    component: LoginSystem,
  },
  {
    path: "/paperdetail/:id",
    name: "PaperDetail",
    component: PaperDetail,
    meta: { requiresAuth: true },
  },
  {
    path: "/formcoordinator/:id",
    name: "FormCoordinator",
    component: FormCoordinator,
  },
  {
    path: "/test",
    name: "TestPage",
    component: TestPage,
  },
  {
    path: "/systemdisplay",
    name: "SystemDisplay",
    component: SystemDisplay,
  },
  {
    path: "/tableconferencescoordinator",
    name: "TableConferencesCoordinator",
    component: TableConferencesCoordinator,
  },
  {
    path: "/regirationrapertable",
    name: "RegirationPaperTable",
    component: RegirationPaperTable,
  },
  {
    path: "/regirationpapername/:id",
    name: "RegirationPaperName",
    component: RegirationPaperName,
  },
  {
    path: "/graphcategory/:id",
    name: "GraphCategory",
    component: GraphCategory,
    meta: { requiresAuth: true },
  },
  {
    path: "/paperallname/:id",
    name: "PaperAllName",
    component: PaperAllName,
    meta: { requiresAuth: true },
  },
  {
    path: "/newcategoryadmin",
    nmae: "NewCategoryAdmin",
    component: NewCategoryAdmin,
    meta: { requiresAuth: true },
  },
  {
    path: "/categorycoordinator",
    nmae: "CategoryCoordinator",
    component: CategoryCoordinator,
  },
  {
    path: "/formregiration/:id",
    name: "FormRegiration",
    component: FormRegiration,
  },
  {
    path: "/:pathMatch(.*)",
    name: "not-found",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const isAuthenticated =
    store.state.isAuthenticated || localStorage.getItem("isAuthenticated");
  if (to.meta.requiresAuth && !isAuthenticated) {
    next("/login");
  } else {
    if (!store.state.isAuthenticated && isAuthenticated) {
      store.commit("setAuthenticationState", true);
    }
    next();
  }
});


export default router;
